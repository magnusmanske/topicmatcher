<?php

require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
require_once ( "/data/project/topicmatcher/scripts/topicmatcher.php") ;

class TopicMatcherTasks extends TopicMatcher {
	public $dbwd ;
	protected $bad_p31 = [ 'Q4167410' , 'Q5' , 'Q3305213' , 'Q19389637' , 'Q13442814' , 'Q179700' , 'Q860861' , 'Q11424' , 'Q12308941' , 'Q11879590' , 'Q202444' , 'Q101352' ] ;
	protected $taxon_name2q = [] ;

	public function __construct () {
		parent::__construct();
		$this->dbwd = $this->tfc->openDB ( 'wikidata' , 'wikidata' ) ;
	}

	public function named_after ( $limit = 10000 ) {
		$offset = 0 ;
		$filter = " FILTER ( ?object_p31!=wd:" . implode ( " && ?object_p31!=wd:" , $this->bad_p31 ) . ") " ;
		while ( 1 ) {
			$sparql = "SELECT ?object ?object_p31 ?human ?label { ?human wdt:P31 wd:Q5 ; rdfs:label ?label FILTER ( lang(?label)='en' ) . ?object wdt:P31 ?object_p31 ; rdfs:label ?label . MINUS { ?object wdt:P138 [] } MINUS { ?object wdt:P921 [] } MINUS { ?object wdt:P180 [] }" ;
			$sparql .= $filter . " } LIMIT {$limit} OFFSET {$offset}" ;
			foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $b ) {
				$object_q = $this->tfc->parseItemFromURL ( $b['object'] ) ;
				$object_p31_q = $this->tfc->parseItemFromURL ( $b['object_p31'] ) ;
				$human_q = $this->tfc->parseItemFromURL ( $b['human'] ) ;
				if ( $object_q == $human_q ) continue ;
				$label = $b['label'] ;
				$item_id = $this->getOrCreateOpenItemID ( $object_q , $object_p31_q , 'Q5' , $label ) ;
				if ( !isset($item_id) ) continue ; # Item exists, but not open
				$this->addTopicForItem ( $item_id , $human_q , 138 ) ;
			}
			$offset += $limit ;
		}
	}

	# Biographical article
	public function biography() {
		$sparql = "select ?q { ?q wdt:P31 wd:Q19389637 MINUS { ?q wdt:P921 [] } }" ;
		$items = $this->tfc->getSPARQLitems ( $sparql , 'q' ) ;
		foreach ( $items AS $q ) {
			$item_id = $this->getOrCreateOpenItemID ( $q , 'Q19389637' , 'Q5' ) ;
			if (!isset($item_id) ) continue ;
			$this->wil = new WikidataItemList ;
			$this->wil->loadItem ( $q ) ;
			$i = $this->wil->getItem ( $q ) ;
			if ( !isset($i) ) continue ; // Item does not exist?

			# Construct search term
			$label = $i->getLabel() ;
			if ( preg_match ( '/^Diccionario Bibliographico Brazileiro\/(.+?)$/' , $label , $m ) ) $label = $m[1] ;
			if ( preg_match ( '/^Galeria dos Brasileiros Ilustres\/(.+?)$/' , $label , $m ) ) $label = $m[1] ;
			if ( preg_match ( '/^.+?:(.+?)$/' , $label , $m ) ) $label = $m[1] ;
			if ( preg_match ( '/^Biograph\S+ \S+ (.+?)$/' , $label , $m ) ) $label = $m[1] ;
			$label = preg_replace ( '/\b(sir|mme|professor|lord|earl|duke|PDBP) /i' , ' ' , $label ) ;
			$label = preg_replace ( '/[0-9\-,]{2,}/' , ' ' , $label ) ;
			if ( preg_match ( '/ (de la|de|von|of|or|and) /' , $label , $m ) ) $label = $m[2] . ' ' . $m[1] ;
			if ( preg_match ( '/^(.+?), (.+)$/' , $label , $m ) ) $label = $m[2] . ' ' . $m[1] ;
			$label = preg_replace ( '/\(.+?\)/' , '' , $label ) ;
			$label = trim ( preg_replace ( '/\s+/' , ' ' , $label ) ) ;
			if ( strlen($label) < 5 ) continue ; // Sanity check
			if ( preg_match ( '/^D\d+$/' , $label ) ) continue ; // Sanity check
			$this->getSQL ( "UPDATE item SET search_label='".$this->escape($label)."' WHERE id={$item_id}" ) ;

			$query = "{$label} haswbstatement:P31=Q5" ; # They're only human, after all
			$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=10&format=json&srsearch=" . urlencode($query) ;
			$j = json_decode ( file_get_contents ( $url ) ) ;
			if ( $j === null or !isset($j) ) continue ;
			foreach ( $j->query->search AS $result ) {
				$topic_q = $result->title ;
				$this->addTopicForItem ( $item_id , $topic_q , 921 ) ;
			}
		}
	}

	# Paintings of people
	public function painting2person() {
		$starts = range('A', 'Z') ;
		foreach ( $starts AS $start ) {
			$sparql = "SELECT DISTINCT ?topic ?painting ?label { ?painting wdt:P31 wd:Q3305213 ; rdfs:label ?label . ?topic wdt:P31 wd:Q5 ; rdfs:label ?label . FILTER ( strstarts(str(?label),'{$start}') ) . MINUS { ?painting wdt:P180 [] } }" ;
			foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $b ) {
				$label = $b['label'] ;
				if ( preg_match ( '/^\S+$/' , $label ) ) continue ; # No single-word ones please!
				$item_q = $this->tfc->parseItemFromURL ( $b['painting'] ) ;
				$topic_q = $this->tfc->parseItemFromURL ( $b['topic'] ) ;
				$item_id = $this->getOrCreateOpenItemID ( $item_q , 'Q3305213' , 'Q5' , $label ) ;
				if ( !isset($item_id) or $item_id == 0 ) continue ;
				$this->addTopicForItem ( $item_id , $topic_q , 180 ) ;
			}
		}
	}

	# Paintings of topics, via image
	public function painting2file2topic() {
		$sparql = 'SELECT ?topic ?topicInstance ?painting  WHERE { ?topic wdt:P31 ?topicInstance . ?topic wdt:P18 ?pic . ?painting wdt:P18 ?pic . ?painting wdt:P31 wd:Q3305213 .   FILTER ( ?topic != ?painting ) OPTIONAL { ?painting wdt:P180 ?depicts } . FILTER (!BOUND(?depicts)) }' ;
		$j = $this->tfc->getSPARQL ( $sparql ) ;
		foreach ( $j->results->bindings AS $b ) {
			$item_q = $this->tfc->parseItemFromURL ( $b['painting'] ) ;
			$topic_q = $this->tfc->parseItemFromURL ( $b['topic'] ) ;
			$topic_instance_q = $this->tfc->parseItemFromURL ( $b['topicInstance'] ) ;
			$item_id = $this->getOrCreateOpenItemID ( $item_q , 'Q3305213' , $topic_instance_q ) ;
			if ( !isset($item_id) or $item_id == 0 ) continue ;
			$this->addTopicForItem ( $item_id , $topic_q , 180 ) ;
		}
	}

	# Papers about subspecies
	public function subspecies() {
		$sparql = 'SELECT ?taxon ?taxonName { ?taxon wdt:P31 wd:Q16521 ; wdt:P105 wd:Q68947 ; wdt:P225 ?taxonName }' ;
		foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $b ) {
			$label = $b['taxonName'] ;
			$taxon_q = $this->tfc->parseItemFromURL ( $b['taxon'] ) ;
			$query = "\"{$label}\" haswbstatement:P31=Q13442814" ; # Scholarly publications with that taxon name
			$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
			$j = json_decode ( file_get_contents ( $url ) ) ;
			if ( $j === null or !isset($j) ) continue ;
			foreach ( $j->query->search AS $result ) {
				$paper_q = $result->title ;
				$item_id = $this->getOrCreateOpenItemID ( $paper_q , 'Q13442814' , 'Q16521' , $label ) ;
				$this->addTopicForItem ( $item_id , $taxon_q , 921 ) ;
			}
		}
	}

	# Streets named after people
	public function streets() {
		$querycache = [] ;
		$sparql = 'select ?street ?streetLabel { ?street wdt:P31 wd:Q79007 MINUS { ?street wdt:P138 [] } SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],it,es,fr,nl,de,en". } }' ;
		foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $b ) {
			$label = $b['streetLabel'] ;
			$street_q = $this->tfc->parseItemFromURL ( $b['street'] ) ;

			if ( preg_match ( '/\d/' , $label ) ) continue ; # No numbers in name
			$label = trim ( preg_replace ( '/\s*\(.*?\)\s*/' , ' ' , $label ) ) ; # remove (...)
			$label = trim ( preg_replace ( '/,.*$/' , ' ' , $label ) ) ; # remove ,
			$name = '' ;
			if ( preg_match ( '/^(.+?[- ].+?)[- ]*(strasse|straße|street|straat|avenue|road|weg|way|row|gasse|damm|markt)$/i' , $label , $m) ) {
				$name = str_replace ( '-' , ' ' , $m[1] ) ;
			} else if ( preg_match ( '/^(Avenue|Avenida|Avinguda|Boulevard) (.+?[- ].+)$/i' , $label , $m) ) {
				$name = str_replace ( '-' , ' ' , $m[2] ) ;
			} else continue ;
			$query = "\"{$name}\" haswbstatement:P31=Q5" ;
			$j = [] ;
			if ( isset($querycache[$query]) ) {
				$j = $querycache[$query] ;
			} else {
				$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
				$j = json_decode ( file_get_contents ( $url ) ) ;
				$querycache[$query] = $j ;
			}
			if ( $j === null or !isset($j) or $j->query->searchinfo->totalhits == 0 ) continue ;
			foreach ( $j->query->search AS $result ) {
				$person_q = $result->title ;
				$item_id = $this->getOrCreateOpenItemID ( $street_q , 'Q79007' , 'Q5' , $label ) ;
				$this->addTopicForItem ( $item_id , $person_q , 138 ) ;
			}
		}
	}


	protected function parsePaperTitle ( $o , &$candidates ) {
		if ( !preg_match_all ( '/\(([A-Z][a-z]+ [a-z ]+)( L){0,1}\.{0,1}\)/' , $o->term_text , $m ) ) return ;
		$candidates[] = [ 'q' => $o->page_title , 'taxa' => $m[1] ] ;
	}

	protected function parsePaperTitle2 ( $o , &$candidates ) {
		if ( !preg_match_all ( '/([A-Z][a-z ]+?(, *| and *| *\(\S+?:))/' , $o->term_text , $m ) ) return ;
		$taxa = [] ;
		foreach ( $m[1] AS $taxon ) {
			$taxon = trim ( preg_replace ( '/(, *| and *| *\(\S+?:)$/' , '' , $taxon ) ) ;
			if ( preg_match ( '/ (in|or|and|of|the|level) /' , $taxon ) ) continue ;
			if ( preg_match ( '/\b(species|growth|india|china|asia|brazil|utah|germany|america|phylogeny|city|tree|resin|composition|africa|province|parasitioogy|mexico|fauna|territory|europe)\b/i' , $taxon ) ) return ;
			if ( preg_match ( '/^(A|The) /i' , $taxon ) ) continue ;
			if ( count ( explode ( ' ' , $taxon ) ) > 3 ) continue ;
			$taxa[] = $taxon ;
		}
		if ( count($taxa) == 0 ) return ;
		$candidates[] = [ 'q' => $o->page_title , 'taxa' => $taxa ] ;
	}

	protected function processCandidates ( $candidates ) {
		$look_up_taxon = [] ;
		$paper_qs = [] ;
		foreach ( $candidates AS $c ) {
			$paper_qs[] = $c['q'] ;
			foreach ( $c['taxa'] AS $taxon ) {
				if ( isset($this->taxon_name2q[$taxon]) ) continue ;
				$look_up_taxon[] = $taxon ;
				$this->taxon_name2q[$taxon] = '' ; // Empty placeholder
			}
		}
		$wil = new WikidataItemList ;
		$wil->loadItems ( $paper_qs ) ;
		if ( count($look_up_taxon) > 0 ) {
			$sparql = "SELECT ?q ?name { VALUES ?name { '" . implode("' '",$look_up_taxon) . "' } ?q wdt:P31 wd:Q16521 ; wdt:P225 ?name }" ;
			$counts = [] ;
			$t2q = [] ;
			foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $b ) {
				$q = $this->tfc->parseItemFromURL ( $b['q'] ) ;
				$name = $b['name'] ;
				if ( isset($counts[$name]) ) $counts[$name]++ ;
				else $counts[$name] = 1 ;
				$t2q[$name] = $q ;
			}
			foreach ( $counts AS $name => $cnt ) { // Make sure we don't use taxon names with more than one item
				if ( $cnt == 1 ) $this->taxon_name2q[$name] = $t2q[$name] ;
			}
		}

		$commands = [] ;
		foreach ( $candidates AS $c ) {
			$paper_q = $c['q'] ;
			$i = $wil->getItem ( $paper_q ) ;
			if ( !isset($i) ) continue ;
			foreach ( $c['taxa'] AS $taxon ) {
				$topic_q = $this->taxon_name2q[$taxon] ;
				if ( !isset($topic_q) ) continue ; // Paranoia
				if ( $this->isBadTopicQ($topic_q,"P921") ) continue ;
				if ( $topic_q == '' ) {
					$this->getSQL ( "INSERT IGNORE INTO paper2topic (paper_q,topic) VALUES ('{$paper_q}','" . $this->escape($taxon) . "')" ) ;
	#				$this->addSearchResultsAsTopicForItem ( $paper_q , $taxon , 921 , 'Q13442814' , 'Q16521' ) ;
					continue ;
				}
				if ( $i->hasTarget ( 'P921' , $topic_q ) ) continue ;
				$commands[] = "{$paper_q}	P921	{$topic_q}" ;
			}
		}
		if ( count($commands) == 0 ) return ;
		$this->tfc->runCommandsQS ( $commands ) ;
	}

	protected function isBadTopicQ ( $topic_q , $property ) {
		if ( $property=="P921" ) {
			if ( in_array($topic_q, ["Q428243"]) ) return true ;
		}
		return false ;
	}

	protected function getPaperSQL ( $like , $page_id , $max_papers ) {
		$sql = '
			SELECT
				page_id,page_title,wbx_text AS term_text
			FROM wbt_item_terms,page,pagelinks,wbt_text,wbt_term_in_lang,wbt_text_in_lang
			WHERE
				wbit_term_in_lang_id = wbtl_id AND
				wbtl_type_id = 1 /* label */ AND
				wbtl_text_in_lang_id = wbxl_id AND
				wbxl_text_id = wbx_id AND 
				wbx_text LIKE "' . $like . '" AND
				wbit_item_id=substr(page_title,2) AND
				page_namespace=0 AND
				pl_from=page_id AND 
				pl_namespace=0 AND 
				pl_from_namespace=0 AND
				pl_title="Q13442814" AND
				pl_from>' . $page_id . '
			ORDER BY pl_from
			LIMIT ' . $max_papers ;
		return trim($sql) ;
	}

	# Papers mentioning (sub)species
	public function paper2taxon() {
		$max_papers = 10000 ;

		// Using page_id as a marker to know where to start next time
		$page_id = 0 ; // Default; to-do: store last one processed
		$sql = "SELECT * FROM kv WHERE key_name='topicmatcher_paper2taxon_last_page_id'" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) $page_id = $o->value * 1 ;

		$sql = $this->getPaperSQL ( '%(%' , $page_id , $max_papers ) ;
		$qs = $this->tfc->getQS ( 'topicmatcher_paper2taxon' , '/data/project/topicmatcher/bot.ini' ) ;
		$candidates = [] ;
		$result = $this->tfc->getSQL ( $this->dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $o->page_id > $page_id ) $page_id = $o->page_id ;
			$this->parsePaperTitle ( $o , $candidates ) ;
			if ( count($candidates) >= 50 ) {
				$this->processCandidates ( $candidates ) ;
				$candidates = [] ;
			}
		}
		$this->processCandidates ( $candidates ) ;

		// remember $page_id
		$sql = "REPLACE INTO kv (key_name,value) VALUES ('topicmatcher_paper2taxon_last_page_id','{$page_id}')" ;
		$this->getSQL ( $sql ) ;

	}

	public function paper2taxon2() {
		$max_papers = 10000 ;

		// Using page_id as a marker to know where to start next time
		$page_id = 0 ; // Default; to-do: store last one processed
		$sql = "SELECT * FROM kv WHERE key_name='topicmatcher_paper2taxon2_last_page_id'" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) $page_id = $o->value * 1 ;

		$sql = $this->getPaperSQL ( '%(%:%)%' , $page_id , $max_papers ) ;

		$qs = $this->tfc->getQS ( 'topicmatcher_paper2taxon2' , '/data/project/topicmatcher/bot.ini' ) ;
		$candidates = [] ;
		$result = $this->tfc->getSQL ( $this->dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $o->page_id > $page_id ) $page_id = $o->page_id ;
			$this->parsePaperTitle2 ( $o , $candidates ) ;
			if ( count($candidates) >= 50 ) {
				$this->processCandidates ( $candidates ) ;
				$candidates = [] ;
			}
		}
		$this->processCandidates ( $candidates ) ;

		// remember $page_id
		$sql = "REPLACE INTO kv (key_name,value) VALUES ('topicmatcher_paper2taxon2_last_page_id','{$page_id}')" ;
		$this->getSQL ( $sql ) ;
	}

	public function external_id() {
		$properties = [ 'P628' ] ;
		$qs = $this->tfc->getQS ( 'topicmatcher_external_ids' , '/data/project/topicmatcher/bot.ini' ) ;
		foreach ( $properties AS $prop ) {
			$sparql = "SELECT ?q ?id ?label { ?q wdt:{$prop} ?id ; rdfs:label ?label FILTER ( LANG(?label)='en') }" ;
			foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $b ) {
				if ( $b['label'] == '' or $b['label'] == $b['id'] ) continue ;
				$target_q = $this->tfc->parseItemFromURL ( $b['q'] ) ;
				$query = "{$b['id']} \"{$b['label']}\" haswbstatement:P31=Q13442814" ;
				$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
				$j = json_decode ( file_get_contents ( $url ) ) ;
				if ( $j === null or !isset($j) ) continue ;
				$commands = [] ;
				$this->wil = new WikidataItemList ;
				foreach ( $j->query->search AS $result ) {
					$paper_q = $result->title ;
					$this->wil->loadItems ( [$paper_q] ) ;
					$i = $this->wil->getItem ( $paper_q ) ;
					if ( !isset($i) ) continue ; # Paranoia
					if ( $i->hasTarget ( 'P921' , $target_q) ) continue ; # Already in there
					if ( $this->hasPropertyEverBeenRemovedFromItem ( $paper_q , 'P921' ) ) continue ; # a P921 was once removed, stay away!
					$commands[] = "{$paper_q}	P921	{$target_q}" ;
				}
				if ( count($commands) > 0 ) $this->tfc->runCommandsQS ( $commands , $qs ) ;
			}
		}
	}

	public function errata() {
		$commands = [] ;
		$qs = $this->tfc->getQS ( 'topicmatcher_errata' , '/data/project/topicmatcher/bot.ini' ) ;
		foreach ( ['erratum','corrigendum'] AS $keyword ) {
			$query = "{$keyword} haswbstatement:P31=Q13442814 -haswbstatement:P31=Q1348305" ;
	#________________________________________________
			$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
			$j = json_decode ( file_get_contents ( $url ) ) ;
			if ( $j === null or !isset($j) ) continue ;
			foreach ( $j->query->search AS $result ) {
				$paper_q = $result->title ;
				if ( $this->hasPropertyEverBeenRemovedFromItem ( $paper_q , 'P31' ) ) continue ;
				$commands[] = "{$paper_q}	P31	Q1348305" ;
			}
	#________________________________________________
		}
		if ( count($commands) > 0 ) $this->tfc->runCommandsQS ( $commands , $qs ) ;
	}

	public function obituary() {
		$max_papers = 100 ;
		$qs = $this->tfc->getQS ( 'topicmatcher_obituary' , '/data/project/topicmatcher/bot.ini' ) ;

		$result = $this->getSQL ( "SELECT * FROM kv WHERE key_name='topicmatcher_obituary_last_page_id'" ) ;
		$o = $result->fetch_object() ;
		$page_id = $o->value * 1 ;

		$sql = $this->getPaperSQL ( 'Obituary%' , $page_id , $max_papers ) ;
		$result = $this->tfc->getSQL ( $this->dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( $o->page_id > $page_id ) $page_id = $o->page_id ;
			$paper_q = $o->page_title ;
			if ( !preg_match ( '/^Obituary(\s*[nN]otice){0,1}[ \.:\-]*(.+)$/i' , $o->term_text , $m ) ) continue ;

			$this->wil = new WikidataItemList ;
			$this->wil->loadItems ( [$paper_q] ) ;
			$i = $this->wil->getItem ( $paper_q ) ;
			if ( !isset($i) ) continue ;
			if ( $i->hasClaims ( 'P921' ) ) continue ; // An obituary would likely have only one target...

			if ( !$i->hasClaims ( 'P136' ) ) { # Set genre
				$commands = [ "{$paper_q}	P136	Q309481" ] ;
				$this->tfc->runCommandsQS ( $commands , $qs ) ;
			}

			$name = $m[2] ;
			$born = '' ;
			$died = '' ;
			$name = preg_replace ( '/^.*\b[Rr]emembrance\b/' , '' , $name ) ;
			$name = preg_replace ( '/^(of|for)\s*/' , '' , $name ) ;
			$name = preg_replace ( '/^(Dr|Prof|Professor|Mr|Mme|Sir)+\b\.*\s*/i' , '' , $name ) ; # Titles
			$name = preg_replace ( '/\b[A-Z]\./' , ' ' , $name ) ; # Initials
			$name = preg_replace ( '/( MD| KCVO| OBE| FRCS| Bsc)+$/i' , '' , $name ) ;
			if ( preg_match ( '/(\d{4}).+(\d{4})/' , $name , $m ) ) {
				$born = $m[1] ;
				$died = $m[2] ;
			}
			$name = trim ( preg_replace ( '/[\.,;:\(0-9].*$/' , '' , $name ) ) ;
			if ( !preg_match ( '/ /' , $name ) and strlen($name) < 7 ) continue ; // At least two names (with a space), or a long name, required for search
			
			# Search for name
			$query = "{$name} haswbstatement:P31=Q5" ;
			$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
			$j = json_decode ( file_get_contents ( $url ) ) ;
			if ( $j === null or !isset($j) ) continue ;
			$human_qs = [] ;
			foreach ( $j->query->search AS $r ) {
				$human_qs[] = $r->title ;
			}
			if ( count($human_qs) == 0 ) {
				$item_id = $this->getOrCreateOpenItemID ( $paper_q , 'Q13442814' , 'Q5' , $name ) ; // Add as blank topic?
				continue ;
			}
			if ( $born == '' and count($human_qs) > 10 ) continue ; # Screw that!

			# Try to find specific one
			if ( $born != '' ) {
				$sparql = "SELECT ?q { VALUES ?q { wd:" . implode(" wd:",$human_qs) . " } . ?q wdt:P569 ?born ; wdt:P570 ?died  FILTER ( year(?born)={$born} && year(?died)={$died} ) }" ;
				$items = $this->tfc->getSPARQLitems ( $sparql , 'q' ) ;
				if ( count($items) == 1 ) {
					$commands = [ "{$paper_q}	P921	{$items[0]}" ] ;
					$this->tfc->runCommandsQS ( $commands , $qs ) ;
					continue ;
				}
			}

			$item_id = $this->getOrCreateOpenItemID ( $paper_q , 'Q13442814' , 'Q5' , $name ) ;
			foreach ( $human_qs AS $q ) {
				$this->addTopicForItem ( $item_id , $q , 921 ) ;
			}
		}

		// remember $page_id
		$sql = "REPLACE INTO kv (key_name,value) VALUES ('topicmatcher_obituary_last_page_id','{$page_id}')" ;
		$this->getSQL ( $sql ) ;
	}

	public function topic_query() {
		$elements = [
			'health_problems' => [ 'sparql'=>'select ?q ?label { ?q wdt:P31/wdt:P279* wd:Q2057971 ; rdfs:label ?label FILTER ( LANG(?label) = "en" && regex(str(?label)," \\\\S{3,} ") ) } ORDER BY DESC(strlen(?label))' ]
	#		[ 'sparql'=>"select ?q ?label { VALUES ?q { wd:Q11627066} . ?q rdfs:label ?label FILTER ( LANG(?label) = 'en' ) }"] # TESTING
		] ;
		$qs = $this->tfc->getQS ( 'topicmatcher_query' , '/data/project/topicmatcher/bot.ini' ) ;
		foreach ( $elements AS $group => $element ) {
			if ( isset($argv[2]) and $argv[2] != $group ) continue ; // Run only specific group
			foreach ( $this->tfc->getSPARQL_TSV($element['sparql']) AS $b ) {
				$topic_label = $b['label'] ;
				$topic_q = $this->tfc->parseItemFromURL ( $b['q'] ) ;
				if ( $topic_label == '' or $topic_label == $topic_q ) continue ;

				$query = "\"{$topic_label}\" haswbstatement:P31=Q13442814" ;
				$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
				$j = json_decode ( file_get_contents ( $url ) ) ;
				if ( $j === null or !isset($j) ) continue ;
				$this->wil = new WikidataItemList ;
				$commands = [] ;
				foreach ( $j->query->search AS $result ) {
					$paper_q = $result->title ;
					$this->wil->loadItems ( [$paper_q] ) ;
					$i = $this->wil->getItem ( $paper_q ) ;
					if ( !isset($i) ) continue ; # Paranoia
					if ( $i->hasTarget ( 'P921' , $topic_q) ) continue ; # Already in there
					if ( $this->hasPropertyEverBeenRemovedFromItem ( $paper_q , 'P921' ) ) continue ; # a P921 was once removed, stay away!
					$commands[] = "{$paper_q}	P921	{$topic_q}" ;
				}

				if ( count($commands) > 0 ) $this->tfc->runCommandsQS ( $commands , $qs ) ;
			}
		}
	}

	public function main_subject_but_not_decribed_by() {
		$qs = $this->tfc->getQS ( 'topicmatcher_described_by_source' , '/data/project/topicmatcher/bot.ini' ) ;
		$sparql = 'SELECT ?pub ?topic { ?pub wdt:P31 wd:Q13442814 ; wdt:P921 ?topic . ?topic wdt:P31 wd:Q5 . MINUS { ?topic wdt:P1343 ?pub } } LIMIT 50' ;
		foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $b ) {
			$pub_q = $this->tfc->parseItemFromURL ( $b['pub'] ) ;
			$topic_q = $this->tfc->parseItemFromURL ( $b['topic'] ) ;
			if ( $this->hasPropertyEverBeenRemovedFromItem ( $pub_q , 'P921' , $topic_q ) ) continue ;
			$this->tfc->runCommandsQS ( ["{$topic_q}	P1343	{$pub_q}"] , $qs ) ;
		}
	}

	public function decribed_by_but_not_main_subject() { # Can run  regularly
		$sparql = 'SELECT ?pub ?topic { ?pub wdt:P31 wd:Q13442814 . ?topic wdt:P1343 ?pub . MINUS { ?topic wdt:P921 ?topic } }' ;
		foreach ( $this->tfc->getSPARQL_TSV($sparql) AS $b ) {
			$paper_q = $this->tfc->parseItemFromURL ( $b['pub'] ) ;
			$topic_q = $this->tfc->parseItemFromURL ( $b['topic'] ) ;
			$item_id = $this->getOrCreateOpenItemID ( $paper_q , 'Q13442814' , 'Q5' ) ;
			if ( $item_id == 0 ) continue ; // Huh?
			$this->addTopicForItem ( $item_id , $topic_q , 921 ) ;
		}
	}

	public function fix_paper2topic() {
		$topic2q = [
			'Brassica napus' => 'Q177932' ,
			'Gallus domesticus' => 'Q29533959' ,
			'Coturnix coturnix japonica' => 'Q716869' ,
			'Sus scrofa domestica' => 'Q787' ,
			'Boophilus microplus' => 'Q4808965'
		] ;

		$qs = $this->tfc->getQS ( 'topicmatcher_paper2taxon_fixer' , '/data/project/topicmatcher/bot.ini' ) ;
		foreach ( $topic2q AS $topic_label => $topic_q ) {
			$commands = [] ;
			$sql = "SELECT * FROM paper2topic WHERE topic='" . $this->escape($topic_label) . "'" ;
			$result = $this->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) {
				$commands[] = "{$o->paper_q}	P921	{$topic_q}" ;
				$sql = "DELETE FROM paper2topic WHERE id={$o->id}" ;
				$this->getSQL ( $sql ) ;
			}
			if ( count($commands) == 0 ) continue ;
			$this->tfc->runCommandsQS ( $commands , $qs ) ;
		}
	}

}


/*
paper2disease, based on paper2taxon table:
SELECT DISTINCT ?q ?label {
  VALUES ?label { 'Chagas disease'@en }
  BIND (lcase(?label) AS ?label_l).
  ?q (wdt:P31/wdt:P279*) wd:Q12136 .
  {
    { ?q rdfs:label ?label }
  UNION
  { ?q skos:altLabel ?label }
  UNION
  { ?q rdfs:label ?label_l }
  UNION
  { ?q skos:altLabel ?label_l }
    }
  }
*/

/*
# Third-party names and IDs from mix'n'match
# THIS SHOULD PROBABLY NOT BE USED, USE 'external_id' INSTEAD
if ( $actions['mixnmatch'] ) {
	$qs = $this->tfc->getQS ( 'topicmatcher_mixnmatch_ids' , '/data/project/topicmatcher/bot.ini' ) ;
	$catalogs = [24] ;
	foreach ( $catalogs AS $catalog ) {
		# Get manually matched entries
		$url = "https://tools.wmflabs.org/mix-n-match/api.php?query=download2&catalogs={$catalog}&columns=%7B%22exturl%22%3A1%2C%22username%22%3A0%2C%22aux%22%3A0%2C%22dates%22%3A0%2C%22location%22%3A0%2C%22multimatch%22%3A0%7D&hidden=%7B%22any_matched%22%3A0%2C%22firmly_matched%22%3A0%2C%22user_matched%22%3A0%2C%22unmatched%22%3A1%2C%22automatched%22%3A1%2C%22name_date_matched%22%3A0%2C%22aux_matched%22%3A0%2C%22no_multiple%22%3A0%7D&format=json" ;
		$entries = json_decode ( file_get_contents ( $url ) ) ;
		foreach ( $entries AS $e ) {
			if ( !isset($e->q) or $e->q == '' ) continue ; # Paranoia
			$query = "{$e->external_id} \"{$e->name}\" haswbstatement:P31=Q13442814" ;
#________________________________________________
			$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
			$j = json_decode ( file_get_contents ( $url ) ) ;
			if ( $j === null or !isset($j) ) continue ;
			$commands = [] ;
			foreach ( $j->query->search AS $result ) {
				$paper_q = $result->title ;
				$this->wil->loadItems ( [$paper_q] ) ;
				$i = $this->wil->getItem ( $paper_q ) ;
				if ( !isset($i) ) continue ; # Paranoia
				if ( $i->hasTarget ( 'P921' , $e->q) ) continue ; # Already in there
				if ( $this->hasPropertyEverBeenRemovedFromItem ( $paper_q , 'P921' ) ) continue ; # a P921 was once removed, stay away!
				$commands[] = "{$paper_q}	P921	{$e->q}" ;
			}
#________________________________________________
#if ( count($commands) > 0 ) { print_r ( $commands ) ; exit(0); }
			if ( count($commands) > 0 ) $this->tfc->runCommandsQS ( $commands , $qs ) ;
		}
	}
}
*/

?>