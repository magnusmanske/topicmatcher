<?php

require_once ( "/data/project/topicmatcher/public_html/php/ToolforgeCommon.php") ;
require_once ( "/data/project/topicmatcher/public_html/php/wikidata.php") ;

class TopicMatcher {
	public $tfc ;
	public $wil ;
	public $dbt ;

	# Some topics with popular names but unlikely to be a subject for a painting, article etc.
	# This will have to become a database table eventually, but for now...
	public $topic_blacklist = ['Q8026195','Q1744','Q7637252','Q5545915','Q2366114','Q1508517','Q5545912','Q3293192','Q16051732','Q20876117','Q9368992','Q11339902','Q45607423','Q45607423'] ;

	public function __construct () {
		$this->tfc = new ToolforgeCommon ( 'topicmatcher' ) ;
		$this->wil = new WikidataItemList ;
	}

	public function getSQL ( $sql ) {
		$this->getDB() ;
		return $this->tfc->getSQL ( $this->dbt , $sql ) ;
	}

	public function escape ( $s ) {
		$this->getDB() ;
		return $this->dbt->real_escape_string ( $s ) ;
	}

	public function getDB () {
		if ( !isset($this->db) ) {
			$this->dbt = $this->tfc->openDBtool ( 'topicmatcher_p' ) ;
		}
		return $this->dbt ;
	}

	public function isTopicOnBlacklist ( $topic_q ) {
		return in_array ( $topic_q , $this->topic_blacklist ) ;
	}

	public function addTopicForItem ( $item_id , $topic_q , $property = 0 ) {
		$item_id *= 1 ;
		if ( $item_id == 0 ) die ( "Invalid item_id in TopicMatcher::addTopicForItem\n" ) ;
		if ( !preg_match ( '/^[PQ]\d+$/' , $topic_q ) ) die ( "Bad topic_q '{$topic_q}' in TopicMatcher::addTopicForItem\n") ;
		if ( $this->isTopicOnBlacklist($topic_q) ) return ;

		# Add candidate topic
		$sql = "INSERT IGNORE INTO topic (item_id,topic_q,property) VALUES ({$item_id},'{$topic_q}',{$property})" ;
		$this->getSQL ( $sql ) ;
		if ( $this->dbt->affected_rows > 0 ) {
			$this->updateCandidateCounter ( $item_id ) ;
			$this->setItemStatus ( $item_id , 'OPEN' ) ; // We have a new topic, re-open item if necessary
		}
	}

	public function setItemStatus ( $item_id , $status ) {
		$item_id *= 1 ;
		$status = $this->escape ( $status ) ;
		$sql = "UPDATE item SET `status`='{$status}' WHERE id={$item_id}" ;
		$this->getSQL ( $sql ) ;
	}

	public function getOrCreateOpenItemID ( $item_q , $item_instance = '' , $topic_instance_guess = '' , $search_label = '' ) {
		$item_q = $this->escape ( $item_q ) ;
		$item_instance = $this->escape ( $item_instance ) ;
		$topic_instance_guess = $this->escape ( $topic_instance_guess ) ;
		$search_label = $this->escape ( $search_label ) ;
		$result = $this->getSQL ( "SELECT * FROM item WHERE q='{$item_q}' LIMIT 1" ) ;
		if ($o = $result->fetch_object()){
			if ( $o->status != 'OPEN' ) return ; // Exists, but not open
			return $o->id ;
		} else {
			$this->getSQL ( "INSERT IGNORE INTO item (q,item_instance,topic_instance_guess,search_label,random) VALUES ('{$item_q}','{$item_instance}','{$topic_instance_guess}','{$search_label}',rand())" ) ;
			$result = $this->getSQL ( "SELECT * FROM item WHERE q='{$item_q}' LIMIT 1" ) ;
			if (!($o = $result->fetch_object())) return ; // Insert failed
			return $o->id ;
		}
	}

	public function updateCandidateCounter ( $item_id ) {
		$item_id *= 1 ;
		if ( $item_id == 0 ) die ( "Invalid item_id in TopicMatcher::updateCandidateCounter\n" ) ;
		$sql = "UPDATE item SET candidates=(SELECT count(*) FROM topic WHERE item_id=item.id AND status='OPEN') WHERE id={$item_id}" ;
		$this->getSQL ( $sql ) ;
	}

	public function addSearchResultsAsTopicForItem ( $item_q , $query , $property_number , $item_instance = '' , $topic_instance_guess = '' ) {
		$url = "https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=500&format=json&srsearch=" . urlencode($query) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		if ( $j === null or !isset($j) ) return ;
		$search_label = preg_replace ( '/\s*-{0,1}haswbstatement=.*$/' , '' , $query ) ;
		foreach ( $j->query->search AS $result ) {
			$topic_q = $result->title ;
			$item_id = $this->getOrCreateOpenItemID ( $item_q , $item_instance , $topic_instance_guess , $search_label ) ;
			$this->addTopicForItem ( $item_id , $topic_q , $property_number ) ;
		}
	}

	public function hasPropertyEverBeenRemovedFromItem ( $q , $prop , $target_q = '' ) {
		$this->wil->sanitizeQ ( $q ) ;
		$this->wil->sanitizeQ ( $prop ) ;
		$pattern = "Property:{$prop}" ;
		if ( $target_q != '' ) $pattern .= "%[[{$target_q}]]" ;
		$sql = "SELECT count(*) AS cnt from page,revision_compat WHERE page_title='$q' AND page_namespace=0 AND page_id=rev_page AND (" ;
		$sql .= "rev_comment LIKE '%wbremoveclaims-remove%{$pattern}%' OR " ;
		$sql .= "(rev_comment LIKE '/* wb%{$pattern}%' AND rev_user_text='Reinheitsgebot')" ; # Bot added this prop before, byt probably was reverted
		$sql .= ")" ;
		if ( !isset($this->dbwd) ) $this->dbwd = $this->tfc->openDB ( 'wikidata' , 'wikidata' ) ;
		try {
			$result = $this->tfc->getSQL ( $this->dbwd , $sql ) ;
		} catch (Exception $e) {
			return true ; // Fail => safe
		}
		while($o = $result->fetch_object()) {
			if ( $o->cnt >= 1 ) return true ;
		}
		return false ;
	}

} ;

?>