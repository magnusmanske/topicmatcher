#!/usr/bin/php
<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # E_ALL|

require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;
require_once ( "/data/project/topicmatcher/scripts/TopicMatcherTasks.php") ;

$tmt = new TopicMatcherTasks() ;

$actions = [] ;
if ( isset($argv[1]) ) {
	foreach ( explode ( ',' , $argv[1] ) AS $action ) {
		try {
			if ( method_exists($tmt,$action) ) $tmt->$action();
			else print "Unknown action '{$action}'\n" ;
		} catch (Exception $e) {
			echo $e->getMessage() ;
		}
	}
}

?>