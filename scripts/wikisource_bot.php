#!/usr/bin/php
<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # E_ALL|

require_once ( "/data/project/topicmatcher/scripts/wikisource.php") ;

$wm = new WikisourceMatcher ;
$cmd = $argv[1]??'' ;
$wikis = $wm->get_all_wikis() ;
if ( isset($argv[2]) ) $wikis = explode(',',($argv[2]??'')) ;

if ( $cmd == 'import' ) {
	# Import new articles
	foreach ( $wikis AS $wiki ) {
		$wm->import_new_entries($wiki) ;
		$wm->add_main_subjects_from_wikidata ( $wiki ) ;
	}
	$wm->update_p31_guess() ;

	# Import descriptions
	foreach ( $wm->get_all_description_candidates() AS $entry_id ) {
		$description = $wm->getDescriptionFromWikisource ( $o->id ) ;
		$wm->setDescription ( $o->id , $description ) ;
	}

	# Generate automatches
	foreach ( $wm->get_all_automatch_candidates() AS $entry_id ) {
		$wm->automatchEntry ( $entry_id ) ;
	}
} else if ( $cmd == 'missing_subjects' ) {
	foreach ( $wikis AS $wiki ) $wm->add_main_subjects_from_wikidata ( $wiki ) ;
} else if ( $cmd == 'p31_guess' ) {
	$wm->update_p31_guess() ;
} else if ( $cmd == 'descriptions' ) {
	foreach ( $wm->get_all_description_candidates() AS $entry_id ) {
		$description = $wm->getDescriptionFromWikisource ( $entry_id ) ;
		$wm->setDescription ( $entry_id , $description ) ;
	}
} else if ( $cmd == 'automatch' ) {
	foreach ( $wm->get_all_automatch_candidates() AS $entry_id ) {
		$wm->automatchEntry ( $entry_id ) ;
	}
} else if ( $cmd == 'date_match' ) {
	foreach ( $wm->get_all_name_date_candidates() AS $entry_id ) {
		$wm->dateMatchEntry ( $entry_id ) ;
	}
} else if ( $cmd == 'test' ) {
	$entry_id = 98789 ;
	$description = $wm->getDescriptionFromWikisource ( $entry_id ) ;
	print "{$description}\n" ;
}

?>