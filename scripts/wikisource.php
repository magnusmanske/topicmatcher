<?php

require_once ( "/data/project/topicmatcher/scripts/topicmatcher.php") ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

class WikisourceMatcher extends TopicMatcher {
	public $min_description_length = 10 ;
	protected $qs ;
	protected $bad_words = 'Chapter|Part|Hymn|Book|Document|Protocol|Mandala|Volume|Vol' ;
	protected $meta_items = ['Q4167410','Q11266439','Q4167836','Q13406463','Q22808320'] ;
	protected $user_name_date_matcher = 8 ;

	public function __construct () {
		parent::__construct();
	}

	public function get_all_wikis () {
		$ret = [] ;
		$sql = "SELECT `name` FROM `wiki` ORDER BY `name`" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $ret[] = $o->name ;
		return $ret ;
	}

	public function get_p31_guesses () {
		$ret = [] ;
		$sql = "SELECT `p31_guess`,count(*) AS cnt FROM `wikisource` WHERE `p31_guess`!=0 GROUP BY `p31_guess`" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $ret[] = "Q{$o->p31_guess}" ;
		return $ret ;
	}

	public function import_new_entries ( $wiki ) {
		$wiki_id = $this->get_or_create_wiki_id($wiki) ;
		foreach ( $this->get_all_wikisource_pages_batched($wiki) AS $batch ) {
			$titles = [] ;
			foreach ( $batch AS $o ) $titles[$o->page_title] = $this->escape($o->page_title) ;
			$sql = "SELECT `id` FROM `wikisource` WHERE `wiki_id`={$wiki_id} AND `page_title` IN ('" . implode("','",$titles) . "')" ;
			$result = $this->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) unset($titles[$o->page_title]) ;
			foreach ( $batch AS $o ) {
				if ( !isset($titles[$o->page_title]) ) continue ;
				$this->create_new_wikisource_entry ( $wiki_id , $o->page_title , $o->item ) ;
			}
		}
	}

	public function get_list ( $wiki , $status=[] , $order='id' , $p31='' , $page_prefix='' , $offset=0 , $limit=50 ) {
		if ( trim($wiki) == '' ) $wiki_id = 0 ;
		else $wiki_id = $this->get_or_create_wiki_id($wiki) ;
		foreach ( $status AS $k => $v ) $status[$k] = $this->escape(strtoupper($v)) ;
		$p31 = preg_replace('|\D|','',"{$p31}") ;
		if ( $p31=='' ) $p31=0 ;
		$page_prefix_safe = $this->escape($page_prefix) ;
		$offset *= 1 ;
		$limit *= 1 ;
		$sql = "SELECT * FROM `vw_wikisource` " ;

		$conditions = [] ;
		if ( $wiki_id!=0 ) $conditions[] = "wiki_id={$wiki_id}" ;
		if ( $p31>0 ) $conditions[] = "p31_guess={$p31}" ;
		if ( $page_prefix!='' ) $conditions[] = "page_title LIKE '{$page_prefix_safe}%'" ;
		if ( count($status) > 0 ) $conditions[] = "status IN('" . implode("','",$status) . "')" ;
		if ( count($conditions)>0 ) $conditions = " WHERE (". implode(") AND (",$conditions) . ") " ;
		else $conditions = '' ;

		$sql .= $conditions ;
		if ( $order=='title' ) $sql .= "ORDER BY title " ;
		else if ( $order=='page_title' ) $sql .= "ORDER BY page_title " ;
		else $sql .= "ORDER BY id " ;
		$sql .= "LIMIT {$limit} " ;
		if ( $offset > 0 ) $sql .= " OFFSET {$offset} " ;

		global $out ; $out['sql'] = $sql ;

		$ret = ['list'=>[],'max_results'=>0 ] ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$ret['list'][] = $o ;
		}

		# Max available
		$sql = "SELECT count(*) AS `cnt` FROM `wikisource` {$conditions}" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) $ret['max_results'] = $o->cnt*1 ;

		return $ret ;
	}

	public function add_main_subjects_from_wikidata ( $wiki ) {
		$wiki_id = $this->get_or_create_wiki_id($wiki) ;
		$dbwd = $this->tfc->openDB ( 'wikidata' , 'wikidata' ) ;
		foreach ( $this->get_all_missing_subjects_batched($wiki_id,1000) AS $batch ) {
			$items_to_load = [] ;
			$sql = "SELECT `page_title` FROM `page`,`pagelinks` WHERE `page_id`=`pl_from` AND `page_namespace`=0 AND `pl_from_namespace`=0 AND `pl_namespace`=120 AND `pl_title`='P921' AND `page_title` IN ('" . implode("','",$batch) . "')" ;
			$result = $this->tfc->getSQL ( $dbwd , $sql ) ;
			while($o = $result->fetch_object()) $items_to_load[] = $o->page_title ;
			$wil = new WikidataItemList ;
			$wil->loadItems ( $items_to_load ) ;
			foreach ( $batch AS $entry_id => $q ) {
				$item = $wil->getItem ( $q ) ;
				if ( !isset($item) ) continue ;
				$claims = $item->getClaims ( 'P921' ) ;
				if ( count($claims) == 0 ) continue ; # No P921, odd...
				$target = $item->getTarget ( $claims[0] ) ;
				if ( !isset($target) ) continue ; # No target item for P921, odd...
				$target_numeric = preg_replace ( '|\D|' , '' , $target ) * 1 ;
				$sql = "UPDATE `wikisource` SET `main_subject`={$target_numeric},`status`='FROM_WIKIDATA' WHERE `id`={$entry_id} AND `status`!='FROM_WIKIDATA'" ;
				$this->getSQL ( $sql ) ;
			}
		}
	}

	public function update_p31_guess () {
		# Clear
		$sql = "UPDATE `wikisource` SET `p31_guess`=0" ;
		$this->getSQL ( $sql ) ;

		# Human
		$sql = 'UPDATE `wikisource` SET `p31_guess`=5 
			WHERE (title RLIKE "^[A-Z][a-z]+ [A-Z][a-z]+$" OR title RLIKE "^[A-Z][a-z]+ [A-Z][a-z]+ [A-Z][a-z]+$")
			AND title NOT RLIKE "^(Der|Die|Das|Ein|Eine|Einer|Bei|Buch|Brief|Auf|To|For|From|The|Book|Good|Summer|Winter|Spring|Autumn|Santa|Before|After|My|Le|Mother|Father|History|Classical|Modern|First|Second|Third|Fourth|Rise|Early|Late|Chronological|Proper|Animal|United|Christmas|King|Protocol|Home|You|Heart|On|Northern|Southern|Eastern|Western|Fata|Psalmen|Mein|Dein|Napoleons{0,1}|Sonett|Aus|Diocese|Revision|Principality|Abbey|Priory|State|Apostle|Court|Missal|Apostolic|Use|Monastery|Wardenship|Pope|Archdiocese|Name|Taxa|Master|Right|Roman|Bachelor|Mount|Day|Biblical|Cardinal|Bishop|Bible|Picture|University|Capital|Evangelical|Erste|Erster|Zweite|Zweiter|Dritte|Dritter|Canto|Parte|Los) " 
			AND title NOT RLIKE " (and|und|or|oder|nach|den|der|des|einen|einem|einer|eine) "
			AND title NOT RLIKE "(council|colloquy|hour|apostol|dimension|book|island|office|gland|racing|property|spring|alliance|association|circle|acid|synod|falls|oxford|geometr|equation|legal|group|mixed|supplement|iconography|bread|school|controversy|monkey|college|experiment|best|little|big|old|literature|engine|theory|analysis|twelve|prison|company|religion|religious|life|virtue|statistic|masses|requiem|creator|residence|reserved|cases|seven|robber|desert)"
			AND title NOT RLIKE " (Garden|House|Campaign|Education|Expedition|Enigma|Number|Mine|Academy|Theil|Abteilung|Pommern|Calendar|Aggressor|Leute|Freunde|Rune|Band|Brief|War|Revelation|Principle|Disorder|Indians|Armada|Arts|Order|Arms|Bibles|Punishment|Societies|Theology|Basilica|Chant|Hymnography|One|Two|Three|Four|Five|Architecture|Archives*|Republic|Six|Seven|Rite|Chapel|Eight|Courts|Christmas|Beach|Blood|Priory|Days|Pontifices|Rota|Criticism|Crown|Abbey)s{0,1}$"
			AND p31_guess=0' ;
		$this->getSQL ( $sql ) ;

		$sql = 'UPDATE `wikisource` SET `p31_guess`=5 WHERE `p31_guess`=0 AND title RLIKE "^Sir "' ;
		$this->getSQL ( $sql ) ;
	}

	public function getOrCreateUserId ( $username ) {
		$username_safe = $this->escape($username) ;
		$sql = "SELECT `id` FROM `user` WHERE `name`='{$username_safe}'" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) return $o->id * 1 ;
		$sql = "INSERT IGNORE INTO `user` (`name`) VALUES ('{$username_safe}')" ;
		$this->getSQL ( $sql ) ;
		return $this->dbt->insert_id ;
	}

	public function getOrCreateTextId ( $text ) {
		$text = substr ( trim($text) , 0 , 250 ) ; # Max length of text in DB
		if ( $text == '' ) return 0 ; # TODO error?
		$text_safe = $this->escape($text) ;
		$sql = "SELECT `id` FROM `text` WHERE `value`='{$text_safe}'" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) return $o->id * 1 ;
		$sql = "INSERT IGNORE INTO `text` (`value`) VALUES ('{$text_safe}')" ;
		$this->getSQL ( $sql ) ;
		return $this->dbt->insert_id ;
	}

	public function setMainSubject ( $entry_id , $main_subject , $status , $user_id ) {
		$entry = $this->getEntry ( $entry_id ) ;
		$user_id *= 1 ;
		$main_subject = preg_replace('|\D|','',"{$main_subject}") * 1 ;
		if ( $main_subject == $entry->wikidata_item ) $main_subject = '0' ; # Should not have itself as main subject, un-setting
		$status = $this->escape($status) ;
		$ts = $this->tfc->getCurrentTimestamp() ;
		$sql = "UPDATE `wikisource` SET `main_subject`={$main_subject},`status`='{$status}',`user_id`={$user_id},`timestamp`='{$ts}',`needs_automatch`=0 WHERE `id`={$entry_id}" ;
		$this->getSQL ( $sql ) ;
	}

	public function setDescription ( $entry_id , $description ) {
		$entry_id *= 1 ;
		if ( $entry_id <= 0 ) throw new Exception ( __METHOD__.": Entry ID is {$entry_id}" ) ;
		$text_id = $this->getOrCreateTextId ( $description ) ;
		$sql = "UPDATE `wikisource` SET `description_id`={$text_id},`needs_description`=0 WHERE `id`={$entry_id}" ;
		$this->getSQL ( $sql ) ;
		if ( $text_id > 0 ) {
			$sql = "UPDATE `wikisource` SET `needs_name_date_check`=1 WHERE `id`={$entry_id} AND `p31_guess`=5 AND `status` IN ('TODO','AUTOMATCH')" ;
			$this->getSQL ( $sql ) ;
		}
	}

	public function automatchEntry ( $entry_id ) {
		$entry = $this->getEntry ( $entry_id ) ;
		$query = $entry->title ;
		if ( $entry->p31_guess > 0 ) $query .= " haswbstatement:P31=Q{$entry->p31_guess}" ;
		foreach ( $this->meta_items AS $no_p31 ) {
			$query .= " -haswbstatement:P31={$no_p31}" ;
		}
		$url = "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srsearch=" . urlencode($query) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		$qs = [] ;
		foreach ( $j->query->search AS $candidate ) {
			$q = preg_replace ( '|\D|' , '' , $candidate->title ) ;
			if ( $q == $entry->wikidata_item ) continue ; # Avoid self-reference
			$qs[] = $q ;
		}
		if ( count($qs) == 0 ) return ;
		$this->setMainSubject ( $entry_id , $qs[0] , 'AUTOMATCH' , 0 ) ;
	}

	public function dateMatchEntry ( $entry_id ) {
		$entry = $this->getEntry ( $entry_id ) ;

		$sql = "UPDATE `wikisource` SET `needs_name_date_check`=0 WHERE `id`={$entry_id} AND `needs_name_date_check`=1" ;
		$this->getSQL ( $sql ) ;

		if ( $entry->status != 'AUTOMATCH' ) return ; // Not automatch
		if ( $entry->p31_guess != 5 ) return ; // not Q5, probably
		if ( $entry->wikidata_item <= 0 ) return ; # Paranoia
		if ( $entry->main_subject <= 0 ) return ; # Paranoia
		if ( $entry->wikidata_item == $entry->main_subject ) return ; # Can not have itself as main subject
		if ( !preg_match ( '|(\d{4}).+?(\d{4})|' , $entry->description , $m ) ) return ; // No dates
		$born = $m[1] ;
		$died = $m[2] ;

		$q = $entry->main_subject ;
		$wil = new WikidataItemList ;
		$wil->loadItems ( [ "Q{$q}" ] ) ;
		$i = $wil->getItem($q);
		if ( !isset($i) ) return ; # No such item

		if ( $i->getLabel() != $entry->title ) return ; # Name match TODO aliases?

		$date_matches = 0 ;
		foreach ( $i->getClaims('P569') AS $c ) {
			if ( $c->mainsnak->datavalue->value->precision < 9 ) continue ;
			if ( $c->mainsnak->datavalue->value->precision > 11 ) continue ;
			if ( preg_match('|^\+(\d+)|',$c->mainsnak->datavalue->value->time,$m) ) {
				if ( $m[1] == $born ) {
					$date_matches++ ;
					break ;
				}
			}
		}
		foreach ( $i->getClaims('P570') AS $c ) {
			if ( $c->mainsnak->datavalue->value->precision < 9 ) continue ;
			if ( $c->mainsnak->datavalue->value->precision > 11 ) continue ;
			if ( preg_match('|^\+(\d+)|',$c->mainsnak->datavalue->value->time,$m) ) {
				if ( $m[1] == $died ) {
					$date_matches++ ;
					break ;
				}
			}
		}
		if ( $date_matches != 2 ) return ; # Not a date match

		$commands = "Q{$entry->wikidata_item}\tP921\tQ{$q}" ;
		$qs = $this->getQS() ;
		$tmp = $qs->importData ( $commands , 'v1' ) ;
		$qs->runCommandArray ( $tmp['data']['commands'] ) ;
		$this->setMainSubject ( $entry_id , $q , 'DATE_MATCH' , $this->user_name_date_matcher ) ;
		sleep(5);
	}

	public function getQS() {
		return $this->tfc->getQS('topicmatcher:wikisource');
	}

	public function getDescriptionFromWikisource ( $entry_id ) {
		$entry = $this->getEntry ( $entry_id ) ;
		$server = $this->tfc->getWebserverForWiki ( $entry->wiki ) ;
		$url = "https://{$server}/wiki/" . urlencode(str_replace(' ','_',$entry->page_title)) ;
		$html = file_get_contents ( $url ) ;
		$html = @iconv('UTF-8', 'UTF-8//IGNORE', $html) ;
		$dom = new DOMDocument();
		$dom->loadHTML($html);
		$xpath = new DOMXpath($dom);
		# TODO remove class='noprint' elements
		$ret = '' ;
		$tc = [] ;

		$contents = $xpath->query("//span[contains(@class,'pagenum')]");
		if ( $contents->count() > 0 ) $tc[] = trim($contents[0]->textContent) ;

		$contents = $xpath->query("//div[@id='mw-content-text']//p");
		foreach ( $contents AS $c ) $tc[] = trim($c->textContent) ;

		for ( $i = 0 ; $i < $contents->count() ; $i++ ) {
			$t = trim($contents[$i]->textContent);
			$t = trim ( preg_replace ( '|^.+\}|' , '' , $t ) ) ;
			if ( strlen($t) < $this->min_description_length ) continue ;
			if ( preg_match('/(​\.mw-parser-output|Empfohlene Zitierweise:|Wikisource-Seite:)/i',$t) ) continue ;
			if ( preg_match('|Catholic_Encyclopedia|',$entry->page_title) and preg_match('|^\(|',$t) and strlen($t)<50 ) {
				$ret .= "{$t} " ;
				continue ;
			}
			$ret .= $t ;
			break ;
		}
		$ret = str_replace ( "\n" , ' ' , $ret ) ;
		return $ret ;
	}

	public function get_all_automatch_candidates () {
		$sql = "SELECT `id` FROM `wikisource` WHERE `needs_automatch`=1" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			yield $o->id ;
		}
	}

	public function get_all_name_date_candidates () {
		$sql = 'SELECT `id` FROM `vw_wikisource` WHERE `status` IN ("AUTOMATCH","TODO") AND `needs_name_date_check`=1 AND `p31_guess`=5 AND `description` RLIKE "\\\\d{4}.+?\\\\d{4}"' ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			yield $o->id ;
		}
	}

	public function get_all_description_candidates () {
		$sql = "SELECT `id` FROM `wikisource` WHERE `needs_description`=1" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			yield $o->id ;
		}
	}

	protected function getEntry ( $entry_id ) {
		$entry_id *= 1 ;
		if ( $entry_id <= 0 ) throw new Exception ( __METHOD__.": Entry ID is {$entry_id}" ) ;
		$sql = "SELECT * FROM `vw_wikisource` WHERE `id`={$entry_id}" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) {}
		else throw new Exception ( __METHOD__.": No entry in `wikisource` for {$entry_id}" ) ;
		return $o ;
	}

	protected function get_all_missing_subjects_batched ( $wiki_id , $batch_size = 10000 ) {
		$last_id = 0 ;
		while ( 1 ) {
			$sql = "SELECT `id`,`wikidata_item` FROM `wikisource` WHERE `wiki_id`={$wiki_id} AND `id`>{$last_id} AND (`status`='AUTOMATCH' OR `main_subject`=0) ORDER BY `id` LIMIT {$batch_size}" ;
			$result = $this->getSQL ( $sql ) ;
			$ret = [] ;
			while($o = $result->fetch_object()) {
				$ret[$o->id] = "Q{$o->wikidata_item}" ;
				$last_id = $o->id ;
			}
			if ( count($ret) == 0 ) break ;
			yield $ret ;
		}
	}

	protected function guess_title ( $page_title ) {
		$title = preg_replace ( '|^.+[/:]|' , '' , $page_title ) ; # Before '/' or ':"'
		$title = str_replace ( '_' , ' ' , $title ) ; # _ => ' '
		$title = preg_replace ( '| *\(.*$|' , '' , $title ) ; # trailing ()
		if ( preg_match ( '|^([A-Z][a-z]+), ([A-Z].*)$|' , $title , $m ) ) $title = "{$m[2]} {$m[1]}" ; # "Last, First" => "First Last"
		$title = preg_replace ( '|^Nr\.{0,1} *|' , '' , $title ) ;
		#$title = preg_replace ( '|^[0-9\.]+ *|' , '' , $title ) ; # Yeah no
		$title = preg_replace ( '|\s{2,}|' , ' ' , $title ) ;
		return trim($title) ;
	}

	protected function is_valid_title ( $title ) {
		if ( $title == '' ) return false ;
		if ( strlen($title) < 3 ) return false ;
		if ( strstr($title,')')!== false and strstr($title,'(')===false ) return false ;
		if ( preg_match('|^[0-9ixv]+$|i',$title) ) return false ;
		if ( preg_match('|\btable of\b|',$title) ) return false ;
		if ( preg_match('|\btitle page\b|',$title) ) return false ;
		if ( preg_match('|\bcopyright notice\b|',$title) ) return false ;
		if ( preg_match('|\bdedication\b|',$title) ) return false ;
		if ( preg_match('|^[0-9,\.:;\(\)\[\]\{\} ]+$|',$title) ) return false ;
		if ( preg_match('|^('.$this->bad_words.') [0-9IVX]|i',$title) ) return false ;
		if ( preg_match('|^('.$this->bad_words.')$|i',$title) ) return false ;
		return true ;
	}

	protected function create_new_wikisource_entry ( $wiki_id , $page_title , $item ) {
		$wiki_id *= 1 ;
		$page_title_safe = $this->escape($page_title) ;
		$title = $this->guess_title ( $page_title ) ;
		if ( !$this->is_valid_title($title) ) return ;
		$title_safe = $this->escape($title) ;
		$q = preg_replace ( '|\D|' , '' , $item ) * 1 ;
		if ( $q <= 0 ) throw new Exception ( __METHOD__.": Item is '{$item}' for {$wiki_id}:{$title}" ) ;
		$sql = "INSERT IGNORE INTO `wikisource` (`wiki_id`,`page_title`,`wikidata_item`,`random`,`title`) VALUES ({$wiki_id},'{$page_title_safe}',{$q},rand(),'{$title_safe}')" ;
		$this->getSQL ( $sql ) ;
		return $this->dbt->insert_id ;
	}

	protected function get_all_wikisource_pages_batched ( $wiki , $batch_size = 1000 ) {
		$dbws = $this->tfc->openDBwiki($wiki) ;
		$last_id = 0 ;
		while ( 1 ) {
			$sql = "SELECT page_id,page_title,pp_value AS item FROM page,page_props WHERE page_id=pp_page AND page_namespace=0 AND pp_propname='wikibase_item' AND page_is_redirect=0 AND page_title RLIKE '[:/]' AND page_id>{$last_id} ORDER BY page_id LIMIT $batch_size" ;
			$batch = [] ;
			$result = $this->tfc->getSQL ( $dbws , $sql ) ;
			while($o = $result->fetch_object()) {
				$batch[] = $o ;
				$last_id = $o->page_id ;
			}
			if ( count($batch) == 0 ) break ;
			yield $batch ;
		}
	}

	protected function get_or_create_wiki_id ( $wiki ) {
		if ( !preg_match('|wikisource$|',$wiki) ) throw new Exception ( __METHOD__.": '{$wiki}' is not wikisource" ) ;
		$wiki_safe = $this->escape($wiki) ;
		$sql = "SELECT `id` FROM `wiki` WHERE `name`='{$wiki_safe}'" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) return $o->id ;
		$sql = "INSERT INTO `wiki` (`name`) VALUES ('{$wiki_safe}')" ;
		$this->getSQL ( $sql ) ;
		return $this->get_or_create_wiki_id ( $wiki ) ;
	}

}

?>