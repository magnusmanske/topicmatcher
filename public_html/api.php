<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
#ini_set('memory_limit','500M');

require_once ( "/data/project/topicmatcher/public_html/php/oauth.php") ;
require_once ( "/data/project/topicmatcher/scripts/topicmatcher.php") ;

function fin ( $s = '' ) {
	global $out ;
	if ( $s != '' ) $out['status'] = $s ;
	header('Content-Type: application/json');
	print json_encode($out) ;
	exit(0) ;
}

function isLoggedIn() {
	global $oa ;
//	if ( !isset($oa) ) $oa = new MW_OAuth ( 'topicmatcher' , 'wikidata' ) ;
//	return $oa->isAuthOK() ;
	return true ; // TESTING FIXME
}

function getUserName() {
	return $_REQUEST['username'] ;
}

$tm = new TopicMatcher() ;


$action = $tm->tfc->getRequest ( 'action' , '' ) ;

$out = [ "status" => "OK" , "data" => [] ] ;

if ( $action == 'get_random' ) {
	$limit = $tm->tfc->getRequest ( 'limit' , '1' ) * 1 ;
	$q = $tm->tfc->getRequest ( 'q' , '' ) ;
	$topic_q = $tm->tfc->getRequest ( 'topic_q' , '' ) ;
	$topic_p = $tm->tfc->getRequest ( 'topic_p' , 0 ) * 1 ;
	if ( isset($_REQUEST['item_instance']) ) $item_instance = $tm->tfc->getRequest ( 'item_instance' , '' ) ;
	if ( isset($_REQUEST['topic_instance_guess']) ) $topic_instance_guess = $tm->tfc->getRequest ( 'topic_instance_guess' , '' ) ;

	$r = rand() / getrandmax() ;
	$sql = "SELECT * FROM item WHERE random>={$r} AND `status`='OPEN'" ;
	if ( isset($item_instance) ) $sql .= " AND item_instance='" . $tm->escape($item_instance) . "'" ;
	if ( isset($topic_instance_guess) ) $sql .= " AND topic_instance_guess='" . $tm->escape($topic_instance_guess) . "'" ;
	if ( $topic_q != '' ) $sql .= " AND EXISTS (SELECT * FROM topic WHERE item_id=item.id AND topic_q='" . $tm->escape($topic_q) . "')" ;
	if ( $topic_p != 0 ) $sql .= " AND EXISTS (SELECT * FROM topic WHERE item_id=item.id AND property={$topic_p})" ;
	$sql .= " ORDER BY random LIMIT {$limit}" ;

	if ( $q != '' ) $sql = "SELECT * FROM item WHERE q='" . $tm->escape($q) . "'" ;
if ( isset($_REQUEST['testid']) ) $sql = "SELECT * FROM item WHERE id=" . ($_REQUEST['testid']*1) ;

#	$out['sql'] = $sql ;
	$result = $tm->getSQL ( $sql ) ;
	while ($o = $result->fetch_object()){
		$o->topics = [] ;
		$sql = "SELECT * FROM topic WHERE item_id={$o->id} AND `status`='OPEN'" ;
		$result2 = $tm->getSQL ( $sql ) ;
		while ($o2 = $result2->fetch_object()) $o->topics[] = $o2 ;
		$out['data'][] = $o ;
	}

	if ( count($out['data']) == 0 and $q != '' ) {
		$o = (object) [
			'candidates'=>0 ,
			'id'=>0,
			'item_instance'=>'',
			'q'=>$q,
			'search_label'=>'',
			'status'=>'OPEN',
			'topic_instance_guess'=>'',
			'topics'=>[]
		] ;
		$out['data'][] = $o ;
	}

} else if ( $action == 'get_item_status' ) {

	$items = json_decode ( $tm->tfc->getRequest ( 'items' , '[]' ) ) ;
	if ( count($items) == 0 ) fin() ;
	foreach ( $items AS $k => $v ) $items[$k] = $tm->escape ( $v ) ;
	$items = "'" . implode ( "','" , $items ) . "'" ;
#	$out['data'] = $items ; fin() ;
	$sql = "SELECT q,`status`,count(*) AS cnt FROM item WHERE q IN ({$items}) GROUP BY q,status" ;
	$result = $tm->getSQL ( $sql ) ;
	while ($o = $result->fetch_object()){
		$out['data'][$o->q][$o->status] = $o->cnt * 1 ;
	}

} else if ( $action == 'rc' ) {

	$start = $tm->tfc->getRequest ( 'start' , '' ) ;

	$out['data'] = [ 'table2id' => [] , 'events' => [] ] ;
	$sql = "SELECT * FROM events WHERE `timestamp`>'" . $tm->escape($start) . "' ORDER BY `timestamp` DESC LIMIT 50" ;
	$result = $tm->getSQL ( $sql ) ;
	while ($o = $result->fetch_object()){
		$out['data']['events'][] = $o ;
		$out['data']['table2id'][$o->table][$o->table_id] = '' ;
	}
	foreach ( $out['data']['table2id'] AS $table => $ids ) {
		if ( $table == '' ) continue ;
		$sql = "SELECT * FROM `{$table}` WHERE id IN (" . implode(',',array_keys($ids)) . ")" ;
		$result = $tm->getSQL ( $sql ) ;
		while ($o = $result->fetch_object()) $out['data']['table2id'][$table][$o->id] = $o ;
	}

} else if ( $action == 'get_topics_by_q' ) {

	$topic_q = $tm->tfc->getRequest ( 'q' , '' ) ;
	$out['data'] = [ 'topics' => [] , 'items' => [] ] ;

	$items = [] ;
	$sql = "SELECT * FROM topic WHERE topic_q='" . $tm->escape($topic_q) . "' ORDER BY item_id" ;
	$result = $tm->getSQL ( $sql ) ;
	while ($o = $result->fetch_object()) {
		$out['data']['topics'][] = $o ;
		$items[$o->item_id] = $o->item_id ;
	}

	if ( count($items) > 0 ) {
		$sql = "SELECT * FROM item WHERE id IN (" . implode ( "," , $items ) . ")" ;
		$result = $tm->getSQL ( $sql ) ;
		while ($o = $result->fetch_object()) $out['data']['items'][$o->id] = $o ;
	}

} else if ( $action == 'get_info' ) {

	$result = $tm->getSQL ( "SELECT item_instance,count(*) AS cnt FROM item WHERE item_instance!='' GROUP BY item_instance HAVING cnt>=10" ) ;
	while ($o = $result->fetch_object()) $out['data']['item_instance'][] = $o->item_instance ;

	$result = $tm->getSQL ( "SELECT topic_instance_guess,count(*) AS cnt FROM item WHERE topic_instance_guess!='' GROUP BY topic_instance_guess HAVING cnt>=10" ) ;
	while ($o = $result->fetch_object()) $out['data']['topic_instance_guess'][] = $o->topic_instance_guess ;

	$result = $tm->getSQL ( "SELECT DISTINCT property FROM topic WHERE property!=0" ) ;
	while ($o = $result->fetch_object()) $out['data']['property'][] = $o->property ;

} else if ( $action == 'finish_topic' ) {

	$item_id = $tm->tfc->getRequest ( 'item_id' , '0' ) * 1 ;
	if ( $item_id == 0 ) fin () ;
	if ( !isLoggedIn() ) fin ( "Not logged in" ) ;
	$sql = "UPDATE item SET `status`='DONE' WHERE id={$item_id}" ;
	$tm->getSQL ( $sql ) ;
	$sql = "INSERT INTO events (`action`,`user`,`timestamp`,`table`,`table_id`) VALUES ('closed item','".$tm->escape(getUserName())."','".$tm->tfc->getCurrentTimestamp()."','item','{$item_id}')" ;
	$tm->getSQL ( $sql ) ;

} else if ( $action == 'add_topic' ) {

	$topic_id = $tm->tfc->getRequest ( 'topic_id' , '0' ) * 1 ;
	if ( $topic_id == 0 ) fin ( "Bad/missing topic_id" ) ;
	$sql = "UPDATE `topic` SET `status`='DONE' WHERE id={$topic_id}" ;
	$tm->getSQL ( $sql ) ;
	$sql = "INSERT INTO events (`action`,`user`,`timestamp`,`table`,`table_id`) VALUES ('added topic','".$tm->escape(getUserName())."','".$tm->tfc->getCurrentTimestamp()."','topic','{$topic_id}')" ;
	$tm->getSQL ( $sql ) ;
	$sql = "SELECT item_id FROM `topic` WHERE id={$topic_id} LIMIT 1" ;
	$result = $tm->getSQL ( $sql ) ;
	if (!($o = $result->fetch_object())) $tm->updateCandidateCounter ( $o->item_id ) ;

} else if ( $action == 'wikisource_get_info' ) {

	require_once ( "/data/project/topicmatcher/scripts/wikisource.php") ;
	$wm = new WikisourceMatcher ($tm) ;
	$out['data']['wikis'] = $wm->get_all_wikis() ;
	$out['data']['p31'] = $wm->get_p31_guesses() ;

} else if ( $action == 'wikisource_get_list' ) {

	require_once ( "/data/project/topicmatcher/scripts/wikisource.php") ;
	$wm = new WikisourceMatcher ($tm) ;
	$out['data'] = $wm->get_list (
		$tm->tfc->getRequest ( 'wiki' , '' ) ,
		explode ( ',' , $tm->tfc->getRequest ( 'status' , '' ) ) ,
		$tm->tfc->getRequest ( 'order' , '' ) ,
		$tm->tfc->getRequest ( 'p31' , '' ) ,
		str_replace(' ','_',$tm->tfc->getRequest ( 'page_prefix' , '' )) ,
		$tm->tfc->getRequest ( 'offset' , 0 ) * 1,
		$tm->tfc->getRequest ( 'limit' , 0 ) * 1
	) ;

} else if ( $action == 'wikisource_set_main_subject' ) {

	require_once ( "/data/project/topicmatcher/scripts/wikisource.php") ;
	$wm = new WikisourceMatcher ($tm) ;
	if ( !isLoggedIn() ) {
		fin('Not logged in!');
	}
	$username = getUserName () ;
	$user_id = $wm->getOrCreateUserId ( $username ) ;
	$entry_id = $tm->tfc->getRequest ( 'entry_id' , 0 ) * 1 ;
	$main_subject = $tm->tfc->getRequest ( 'main_subject' , 0 ) * 1 ;
	if ( $main_subject == -1 ) $wm->setMainSubject($entry_id,0,'INVALID',$user_id) ;
	else if ( $main_subject == 0 ) $wm->setMainSubject($entry_id,0,'TODO',0) ;
	else $wm->setMainSubject($entry_id,$main_subject,'DONE',$user_id) ;

} else {
	require_once '/data/project/magnustools/public_html/php/Widar.php' ;
	$widar = new \Widar ( 'topicmatcher' ) ;
	$widar->attempt_verification_auto_forward ( 'https://topicmatcher.toolforge.org/' ) ;
	$widar->authorization_callback = 'https://topicmatcher.toolforge.org/api.php' ;
	if ( $widar->render_reponse(true) ) exit(0);

	$out['status'] = 'Bad action ' . $action ;
}

fin() ;

?>