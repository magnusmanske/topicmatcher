'use strict';

let router ;
let app ;
let wd = new WikiData() ;

let working = false ;
let global_helper_props = [] ;
let global_info = {} ;
let globals_loaded = false ;

function load_globals ( callback ) {
    if ( globals_loaded ) return callback() ;
    $.get ( './api.php' , { action:'get_info' } , function (d) {
          global_info = d.data ;
          wd_link_wd = wd ;
          let sparql = 'SELECT ?p { ?p rdf:type wikibase:Property ; wdt:P31 wd:Q44847669 ; wdt:P1630 [] }' ; // Load artwork properties with formatter URL
          wd.loadSPARQLitems ( sparql , function ( d ) {
            global_helper_props = d ;
            let to_load = global_helper_props.splice() ;
            $.each ( global_info.item_instance , function ( k , v ) { to_load.push ( v ) } ) ;
            $.each ( global_info.topic_instance_guess , function ( k , v ) { to_load.push ( v ) } ) ;
            $.each ( global_info.property , function ( k , v ) { to_load.push ( 'P'+v ) } ) ;
            wd.getItemBatch ( to_load , function () {
              globals_loaded = true ;
              callback()
            } ) ;
          } , function(){alert("SPARQL fail!") } ) ;
        } , 'json' ) ;
}

$(document).ready ( function () {
    vue_components.toolname = 'topicmatcher' ;
//    vue_components.components_base_url = 'https://tools.wmflabs.org/magnustools/resources/vue/' ; // For testing; turn off to use tools-static
    Promise.all ( [
        vue_components.loadComponents ( ['wd-date','wd-link','tool-translate','tool-navbar','commons-thumbnail','widar','autodesc','typeahead-search',
            'vue_components/candidate-topic.html',
            'vue_components/missing-topic.html',
            'vue_components/random-page.html',
            'vue_components/topic-page.html',
            'vue_components/rc-page.html',
            'vue_components/author-page.html',
            'vue_components/main-page.html',
            'vue_components/wikisource-page.html',
            'vue_components/section-bar.html',
            ] )
    ] )
    .then ( () => {

        widar_api_url = 'https://topicmatcher.toolforge.org/api.php' ;
        const routes = [
          { path: '/rc', component: RecentChangesPage , props:true },
          { path: '/rc/:start', component: RecentChangesPage , props:true },
          { path: '/random', component: RandomPage , props:true },
          { path: '/topic', component: TopicPage , props:true },
          { path: '/topic/:q', component: TopicPage , props:true },
          { path: '/wikisource', component: WikisourcePage , props:true },
          { path: '/author/:author_q', component: AuthorPage , props:true },
          { path: '/', component: MainPage , props:true },
        ] ;
        router = new VueRouter({routes}) ;
        app = new Vue ( { router } ) .$mount('#app') ;

    } ) ;
} ) ;
